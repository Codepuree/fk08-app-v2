function parse() {
    var newsElements = document.getElementsByClassName('news-liste');

    // Construct empty feed
    var feed = {
        url: document.URL,
        description: document.head.querySelector("[name=description]").content,
        title: document.title,
        items: []
    };

    for (var i = 0; i < newsElements.length; i++) {
        var newsWrapper = newsElements[i];

        var textElement = newsWrapper.getElementsByClassName('news-text')[0];
        var imageWrapper = newsWrapper.getElementsByClassName('news-image')[0];

        var dateElement = textElement.getElementsByClassName('date')[0];
        var titleElement = textElement.getElementsByTagName('strong')[0];
        var articleElement = textElement.getElementsByTagName('p')[0];
        var imageElement = imageWrapper.getElementsByTagName('img')[0];

        // Date
        var date = parseDate(dateElement.innerText);

        // Contents of text-element
        var title = titleElement.getElementsByTagName('a')[0].innerHTML.replace('&gt;', '').trim();
        var link = titleElement.getElementsByTagName('a')[0].href;
        var article = articleElement ? articleElement.innerText : '';

        // Contents of the image
        var imageUrl = imageElement.src;
        var imageAlt = imageElement.alt;
        var imageTitle = imageElement.title;

        var item = {
            date: date,
            title: title,
            link: link,
            content: article,
            description: article,
            image: {
                url: imageUrl,
                alt: imageAlt,
                title: imageTitle
            }
        };

        feed.items.push(item);

        console.log(
            'Date:   ', date,
            '\nTitle:  ', title,
            '\nLink:   ', link,
            '\nArticle:', article,
            '\nImage Url:  ', imageUrl,
            '\nImage Alt:  ', imageAlt,
            '\nImage Title:', imageTitle
        );
    }

    return feed;
}

function parseDate(dateString) {
    dateString = dateString.replace('[', '');
    dateString = dateString.replace(']', '');

    var dateSplit = [];

    if (dateString.indexOf('|') != -1) {
        dateSplit = dateString.split('|');
    } else if (dateString.indexOf('.') != -1) {
        dateSplit = dateString.split('.');
    }

    var formatedDateString = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0];

    var date = new Date(formatedDateString);

    return date;
}